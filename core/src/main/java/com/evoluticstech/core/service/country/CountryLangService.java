package com.evoluticstech.core.service.country;

import com.evoluticstech.core.domain.location.CountryLang;
import com.evoluticstech.core.model.crm.country.CountryLangEntity;
import com.evoluticstech.core.repository.crm.country.CountryLangRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CountryLangService {
    private final CountryLangRepository repository;
    private final ModelMapper modelMapper;

    public CountryLang save(String code, CountryLang countryLang) {
        countryLang.setCode(code);
        return modelMapper.map(repository.save(modelMapper.map(countryLang, CountryLangEntity.class)), CountryLang.class);
    }

    public CountryLang getCountryLangByCountryCode(String countryCode) {
        var lang = repository.findByCode(countryCode);

        return lang.map(entity -> modelMapper.map(entity, CountryLang.class)).orElse(null);
    }
}
