package com.evoluticstech.core.service.country;

import com.evoluticstech.core.domain.location.Country;
import com.evoluticstech.core.domain.location.CreateCountry;
import com.evoluticstech.core.interfaces.CodeAndDescription;
import com.evoluticstech.core.interfaces.CodeAndDescriptionAndIsoNumberAndId;
import com.evoluticstech.core.interfaces.Group;
import com.evoluticstech.core.model.crm.country.CountryEntity;
import com.evoluticstech.core.repository.crm.country.CountryRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Brume
 **/
@Service
@RequiredArgsConstructor
public class CountryService {
    private final CountryRepository repository;
    private final CountryLangService countryLangService;
    private final ModelMapper modelMapper;

    public List<Country> findAll() {
        return repository.findAll()
                .stream()
                .map(countryEntity -> modelMapper
                        .map(countryEntity, Country.class))
                .collect(Collectors.toList());
    }

    public List<Group> getAllCountryGroup() {
        return repository.findAllDistinctGroupBy();
    }

    public List<CodeAndDescriptionAndIsoNumberAndId> getCodeAndDescriptionAndIsoNumberAndId() {
        return repository.findBy();
    }

    public List<CodeAndDescription> getAllCodeAndDescription() {
        return repository.getCodeAndDescriptionBy();
    }


    public List<CodeAndDescription> getCodeAndDescriptionByCode(String code) {
        return repository.getCodeAndDescriptionByCode(code);
    }

    public Country getCountryByCode(String code) {
        var country = repository.findByCode(code);

        return country.map(entity -> modelMapper.map(entity, Country.class)).orElse(null);
    }

    public List<CodeAndDescription> getCodeAndDescriptionByCountryGroup(String countryGroup) {
        return repository.getCodeAndDescriptionByGroup(countryGroup);
    }

    public CreateCountry newCountry(CreateCountry newCountry) {
        var country = save(newCountry.getCountry());

        var code = country.getCode();

        return CreateCountry.builder()
                .countryLang(countryLangService.save(code, newCountry.getCountryLang()))
                .country(country)
                .build();
    }

    public CreateCountry getCountryByCountryCode(String code) {
        var country = getCountryByCode(code);

        if(country == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Country code not found");

        return CreateCountry.builder()
                .countryLang(countryLangService.getCountryLangByCountryCode(code))
                .country(country)
                .build();
    }

    public Country save(Country country) {
        return modelMapper.map(repository.save(modelMapper.map(country, CountryEntity.class)), Country.class);
    }
}
