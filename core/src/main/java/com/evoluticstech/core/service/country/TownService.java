package com.evoluticstech.core.service.country;

import com.evoluticstech.core.domain.location.Town;
import com.evoluticstech.core.interfaces.CodeAndDescription;
import com.evoluticstech.core.model.crm.country.TownEntity;
import com.evoluticstech.core.repository.crm.country.TownRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Brume
 **/
@Service
@RequiredArgsConstructor
public class TownService {
    private final TownRepository repository;
    private final ModelMapper  modelMapper;


    public List<Town> findAlTowns() {
        return repository.findAll()
                .stream()
                .map(townEntity -> modelMapper
                        .map(townEntity, Town.class))
                .collect(Collectors.toList());
    }

    public List<CodeAndDescription> getCodeAndDescriptionByCode(String code) {
        return repository.getCodeAndDescriptionByCode(code);
    }

    public List<CodeAndDescription> getCodeAndDescriptionByCity(String city) {
        return repository.getCodeAndDescriptionByCity( city);
    }

    public Town createTown(Town newTown){
            return save(newTown);
    }

    public Town getTownByTownCode(String townCode){
        var town = repository.findByCode(townCode);

        return town.map(entity -> modelMapper.map(entity, Town.class)).orElse(null);
    }

    public Town save(Town newTown){
        return modelMapper.map(repository.save(modelMapper.map(newTown, TownEntity.class)), Town.class);
    }
}
