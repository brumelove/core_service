package com.evoluticstech.core.service.country;

import com.evoluticstech.core.domain.location.Region;
import com.evoluticstech.core.interfaces.CodeAndDescription;
import com.evoluticstech.core.model.crm.country.RegionEntity;
import com.evoluticstech.core.repository.crm.location.RegionRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Brume
 **/
@Service
@RequiredArgsConstructor
public class RegionService {
    private final CountryLangService countryLangService;
    private final RegionRepository repository;
    private final ModelMapper  modelMapper;


    public List<Region> findAllRegions() {
        return repository.findAll()
                .stream()
                .map(regionEntity -> modelMapper
                        .map(regionEntity, Region.class))
                .collect(Collectors.toList());
    }

    public Region getRegionByRegionCode(String regionCode) {
        var region = repository.findByCode(regionCode);

        return region.map(entity -> modelMapper.map(entity, Region.class)).orElse(null);
    }

    public List<CodeAndDescription> getCodeAndDescriptionByCode(String code) {
        return repository.getCodeAndDescriptionByCode(code);
    }

    public List<CodeAndDescription> getCodeAndDescriptionByCountry(String country) {
        return repository.getCodeAndDescriptionByCountry( country);
    }

    public Region newRegion(Region newRegion){
        if(newRegion.getId() == null){
            return save(newRegion);
        } else {
            return save(newRegion.getId(), newRegion);
        }
    }

    public Region save(Region newRegion){
        return modelMapper.map( repository.save(modelMapper.map(newRegion, RegionEntity.class)) , Region.class);
    }

    public Region save(Long id, Region newRegion){
        newRegion.setId(id);
        return save(newRegion);
    }
}
