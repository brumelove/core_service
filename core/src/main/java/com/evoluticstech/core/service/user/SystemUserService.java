package com.evoluticstech.core.service.user;

import com.evoluticstech.core.domain.users.SystemUser;
import com.evoluticstech.core.enumerations.UserRoleType;
import com.evoluticstech.core.model.crm.user.SystemUserEntity;
import com.evoluticstech.core.repository.crm.user.SystemUserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Brume
 */

@Service
@Slf4j
@RequiredArgsConstructor
public class SystemUserService implements UserDetailsService {
    private final ModelMapper modelMapper;
    private final SystemUserRepository repository;


    public SystemUser findByUserName(String userName) {
        var user = repository.findByUserName(userName);
        return user.map(systemUserEntity -> modelMapper.map(systemUserEntity, SystemUser.class)).orElse(null);
    }

    private SystemUser findByEmail(String email) {
        var user = repository.findByEmail(email);
        return user.map(systemUserEntity -> modelMapper.map(systemUserEntity, SystemUser.class)).orElse(null);
    }

    public SystemUser createUser(SystemUser systemUser) {
        if (findByUserName(systemUser.getUserName()) != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The username " + systemUser.getUserName() + " exists");
        }

        if (findByEmail(systemUser.getEmail()) != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The email " + systemUser.getEmail() + " exists");
        }
        var user = new SystemUser();
        user.setPassword(new BCryptPasswordEncoder().encode(systemUser.getPassword()));
        user.setEmail(systemUser.getEmail());
        user.setUserName(systemUser.getUserName());
        user.setFirstname(systemUser.getFirstname());
        user.setId(systemUser.getId());
        user.setUserRoleType(systemUser.getUserRoleType());
        user.setSurname(systemUser.getSurname());

        var result = create(user);
        systemUser.setId(result.getId());


        return systemUser;
    }

    public SystemUser create(SystemUser systemUser) {
        var response = repository.save(modelMapper.map(systemUser, SystemUserEntity.class));
        log.info("CREATING NEW USER ::: ::: " + response);

        var result =  modelMapper.map(response, SystemUser.class);
        result.setPassword(systemUser.getPassword());

        return result;
    }

    public SystemUser findById(Long id) {
        var response = repository.findById(id);
        return response.map(entity -> modelMapper.map(entity, SystemUser.class)).orElse(null);
    }

    public List<SystemUser> getAllUsers() {
        var response = repository.findAll();
        return response.stream().map(entity -> modelMapper.map(entity, SystemUser.class)).collect(Collectors.toList());
    }

    public SystemUser update(Long id, SystemUser systemUser) {

        var user = findById(id);
        if (user == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The user  with id " + id + " does not exists");
        }
        systemUser.setId(user.getId());
        modelMapper.map(systemUser, user);
        BeanUtils.copyProperties(systemUser, user, "password");

        return create(user);
    }

    public SystemUser updateRole(Long id, UserRoleType roleType) {

        var user = findById(id);
        if (user == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The user  with id " + id + " does not exists");
        }
        user.setUserRoleType(roleType);

        return create(user);
    }

    public SystemUser updatePassword(Long id, String password) {

        var user = findById(id);
        if (user == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The user  with id " + id + " does not exists");
        }
        user.setPassword(new BCryptPasswordEncoder().encode(password));

        return create(user);
    }

    public String delete(Long id) {

        var user = findById(id);
        if (user == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The user  with id " + id + " does not exists");
        }
        repository.delete(modelMapper.map(user, SystemUserEntity.class));

        return "Deleted Successfully";
    }


    @Override
    public UserDetails loadUserByUsername(String userName) {
        var systemUser = findByUserName(userName);
        if (systemUser.getUserName().equals(userName)) {
            GrantedAuthority authority = new SimpleGrantedAuthority(systemUser.getUserRoleType().name());

            return new User(systemUser.getUserName(), systemUser.getPassword(), Collections.singletonList(authority));
        } else throw new UsernameNotFoundException("User with " + userName + "not found");
    }


    public void logout(HttpServletRequest request, HttpServletResponse response) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();

        try {
            UserDetails userDetails = (UserDetails) authentication.getPrincipal();
            String username = userDetails.getUsername();

            log.info("Logging out " + username);

            new SecurityContextLogoutHandler().logout(request, response, authentication);
            SecurityContextHolder.getContext().setAuthentication(null);

            log.info("User " + username + ": logout successful");

        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Unable to logout the user", e);
        }
    }
}
