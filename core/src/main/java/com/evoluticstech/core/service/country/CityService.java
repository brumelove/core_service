package com.evoluticstech.core.service.country;

import com.evoluticstech.core.domain.location.City;
import com.evoluticstech.core.interfaces.CodeAndDescription;
import com.evoluticstech.core.model.crm.country.CityEntity;
import com.evoluticstech.core.repository.crm.country.CityRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Brume
 **/
@Service
@RequiredArgsConstructor
public class CityService {
    private final CityRepository repository;
    private final ModelMapper modelMapper;

    public List<City> findAllCities() {
        return repository.findAll()
                .stream()
                .map(cityEntity -> modelMapper
                        .map(cityEntity, City.class))
                .collect(Collectors.toList());
    }

    public List<CodeAndDescription> getCodeAndDescriptionByCode(String code) {
        return repository.getCodeAndDescriptionByCode(code);
    }

    public List<CodeAndDescription> getCodeAndDescriptionByRegion(String region) {
        return repository.getCodeAndDescriptionByRegion(region);
    }

    public List<CodeAndDescription> getCodeAndDescriptionByCountry(String country) {
        return repository.getCodeAndDescriptionByCountry(country);
    }

    public City newCity(City newCity) {
        return save(newCity);
    }

    public City getCityByCityCode(String cityCode) {
        var city = repository.findByCode(cityCode);

        return city.map(enitity -> modelMapper.map(enitity, City.class)).orElse(null);
    }

    public City save(City newCity) {
        return modelMapper.map(repository.save(modelMapper.map(newCity, CityEntity.class)), City.class);
    }
}
