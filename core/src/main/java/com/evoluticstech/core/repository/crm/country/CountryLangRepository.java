package com.evoluticstech.core.repository.crm.country;

import com.evoluticstech.core.model.crm.country.CountryLangEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CountryLangRepository extends JpaRepository<CountryLangEntity, Long> {
    Optional<CountryLangEntity> findByCode(String countryCode);
}
