package com.evoluticstech.core.repository.crm.country;

import com.evoluticstech.core.interfaces.CodeAndDescription;
import com.evoluticstech.core.model.crm.country.TownEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Brume
 **/
@Repository
public interface TownRepository extends JpaRepository<TownEntity, Long> {
    List<CodeAndDescription> getCodeAndDescriptionByCity(String city);
    List<CodeAndDescription> getCodeAndDescriptionByCode(String code);
    Optional<TownEntity> findByCode(String code);
}
