package com.evoluticstech.core.repository.crm.location;

import com.evoluticstech.core.interfaces.CodeAndDescription;
import com.evoluticstech.core.model.crm.country.RegionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Brume
 **/
@Repository
public interface RegionRepository extends JpaRepository<RegionEntity, Long> {
    List<CodeAndDescription> getCodeAndDescriptionByCode(String code);
    List<CodeAndDescription> getCodeAndDescriptionByCountry(String country);

    List<RegionEntity> findAllByCountry(String country);

    Optional<RegionEntity> findByCode(String code);
}