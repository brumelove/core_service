package com.evoluticstech.core.repository.crm.country;

import com.evoluticstech.core.interfaces.CodeAndDescription;
import com.evoluticstech.core.interfaces.CodeAndDescriptionAndIsoNumberAndId;
import com.evoluticstech.core.interfaces.Group;
import com.evoluticstech.core.model.crm.country.CountryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Brume
 **/
@Repository
public interface CountryRepository extends JpaRepository<CountryEntity, Long> {
    List<CodeAndDescriptionAndIsoNumberAndId> findBy();
    List<CodeAndDescription> getCodeAndDescriptionByCode(String code);
    List<CodeAndDescription> getCodeAndDescriptionBy();
    List<Group> findAllDistinctGroupBy();
    List<CodeAndDescription> getCodeAndDescriptionByGroup(String countryGroup);
    Optional<CountryEntity> findByCode(String code);
}
