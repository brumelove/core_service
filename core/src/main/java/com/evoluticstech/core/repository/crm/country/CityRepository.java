package com.evoluticstech.core.repository.crm.country;

import com.evoluticstech.core.interfaces.CodeAndDescription;
import com.evoluticstech.core.model.crm.country.CityEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Brume
 **/
@Repository
public interface CityRepository extends JpaRepository<CityEntity, Long> {
    List<CodeAndDescription> getCodeAndDescriptionByCode(String code);
    List<CodeAndDescription> getCodeAndDescriptionByRegion(String region);
    List<CodeAndDescription> getCodeAndDescriptionByCountry(String country);

    List<CityEntity> findAllByRegion(String region);

    Optional<CityEntity> findByCode(String code);
}
