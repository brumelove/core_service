package com.evoluticstech.core.repository.crm.user;

import com.evoluticstech.core.model.crm.user.SystemUserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Brume
 */
@Repository
@Transactional
public interface SystemUserRepository extends JpaRepository<SystemUserEntity, Long>,  QuerydslPredicateExecutor<SystemUserEntity>{

    Optional<SystemUserEntity> findByUserNameOrEmail(String userName, String email);
    Optional<SystemUserEntity> findByUserName(String userName);
    Optional<SystemUserEntity> findByEmail( String email);
}
