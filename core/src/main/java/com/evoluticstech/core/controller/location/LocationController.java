package com.evoluticstech.core.controller.location;

import com.evoluticstech.core.domain.location.*;
import com.evoluticstech.core.interfaces.CodeAndDescription;
import com.evoluticstech.core.interfaces.CodeAndDescriptionAndIsoNumberAndId;
import com.evoluticstech.core.interfaces.Group;
import com.evoluticstech.core.service.country.CityService;
import com.evoluticstech.core.service.country.CountryService;
import com.evoluticstech.core.service.country.RegionService;
import com.evoluticstech.core.service.country.TownService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequiredArgsConstructor
@RequestMapping(value = "/rest/location/")
public class LocationController {
    private final RegionService regionService;
    private final CityService cityService;
    private final TownService townService;
    private final CountryService countryService;


    @GetMapping("country")
    public ResponseEntity<List<Country>> getCountry() {
        return ResponseEntity.ok().body(countryService.findAll());
    }

    @GetMapping("country/groups")
    public ResponseEntity<List<Group>> getAllCountryGroups() {
        return ResponseEntity.ok().body(countryService.getAllCountryGroup());
    }

    @GetMapping("country/group/{countryGroup}")
    public ResponseEntity<List<CodeAndDescription>> getAllCountryCodeAndDescriptionByCountryGroup(@PathVariable final String countryGroup) {
        return ResponseEntity.ok().body(countryService.getCodeAndDescriptionByCountryGroup(countryGroup));
    }

    @GetMapping("country/info/{countryCode}")
    public ResponseEntity<CreateCountry> getCountryByCountryCode(@PathVariable final String countryCode) {
        return ResponseEntity.ok().body(countryService.getCountryByCountryCode(countryCode));
    }

    @GetMapping("country/code/desc/iso/id")
    public ResponseEntity<List<CodeAndDescriptionAndIsoNumberAndId>> getCountryWithCodeDescIsoAndId() {
        return ResponseEntity.ok().body(countryService.getCodeAndDescriptionAndIsoNumberAndId());
    }

    @GetMapping("country/code/desc")
    public ResponseEntity<List<CodeAndDescription>> getAllCodeAndDescription() {
        return ResponseEntity.ok().body(countryService.getAllCodeAndDescription());
    }

    @GetMapping("region")
    public ResponseEntity<List<Region>> getRegion() {
        return ResponseEntity.ok().body(regionService.findAllRegions());
    }

    @GetMapping("region/info/{regionCode}")
    public ResponseEntity<Region> getRegionByRegionCode(@PathVariable final String regionCode) {
        return ResponseEntity.ok().body(regionService.getRegionByRegionCode(regionCode));
    }

    @GetMapping("region/{code}")
    public ResponseEntity<List<CodeAndDescription>> getRegionByCode(@PathVariable final String code) {
        return ResponseEntity.ok().body(regionService.getCodeAndDescriptionByCode(code));
    }

    @GetMapping("region/country/{country}")
    public ResponseEntity<List<CodeAndDescription>> getRegionByCountry(@PathVariable final String country) {
        return ResponseEntity.ok().body(regionService.getCodeAndDescriptionByCountry(country));
    }

    @GetMapping("state")
    public ResponseEntity<List<City>> getState() {
        return ResponseEntity.ok().body(cityService.findAllCities());
    }

    @GetMapping("state/{region}")
    public ResponseEntity<List<CodeAndDescription>> getStateByRegion(@PathVariable final String region) {
        return ResponseEntity.ok().body(cityService.getCodeAndDescriptionByRegion(region));
    }

    @GetMapping("state/info/{cityCode}")
    public ResponseEntity<City> getStateByCityCode(@PathVariable final String cityCode) {
        return ResponseEntity.ok().body(cityService.getCityByCityCode(cityCode));
    }

    @GetMapping("state/country/{country}")
    public ResponseEntity<List<CodeAndDescription>> getStateByCountry(@PathVariable final String country) {
        return ResponseEntity.ok().body(cityService.getCodeAndDescriptionByCountry(country));
    }

    @GetMapping("lga")
    public ResponseEntity<List<Town>> getLGA() {
        return ResponseEntity.ok().body(townService.findAlTowns());
    }

    @GetMapping("town/{city}")
    public ResponseEntity<List<CodeAndDescription>> getTownByCity(@PathVariable final String city) {
        return ResponseEntity.ok().body(townService.getCodeAndDescriptionByCity(city));
    }

    @GetMapping("town/info/{townCode}")
    public ResponseEntity<Town> getTownByTownCode(@PathVariable final String townCode) {
        return ResponseEntity.ok().body(townService.getTownByTownCode(townCode));
    }

    /**
     * POST SECTION
     *
     * @param
     * @return
     */

    @PostMapping("country")
    public ResponseEntity<CreateCountry> createNewCountry(@RequestBody CreateCountry country) {
        return ResponseEntity.ok().body(countryService.newCountry(country));
    }

    @PostMapping("town")
    public ResponseEntity<Town> createNewTown(@RequestBody Town town) {
        return ResponseEntity.ok().body(townService.createTown(town));
    }

    @PostMapping("city")
    public ResponseEntity<City> createNewCity(@RequestBody City city) {
        return ResponseEntity.ok().body(cityService.newCity(city));
    }

    @PostMapping("region")
    public ResponseEntity<Region> createNewRegion(@RequestBody Region region) {
        return ResponseEntity.ok().body(regionService.newRegion(region));
    }
}
