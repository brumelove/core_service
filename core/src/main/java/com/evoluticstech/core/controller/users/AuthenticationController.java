
package com.evoluticstech.core.controller.users;

import com.evoluticstech.core.domain.users.LoginRequest;
import com.evoluticstech.core.domain.users.SystemUser;
import com.evoluticstech.core.enumerations.UserRoleType;
import com.evoluticstech.core.service.user.SystemUserService;
import com.evoluticstech.core.util.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Brume
 */
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequiredArgsConstructor
@Slf4j
@RequestMapping(value = "/rest/authentication")
public class AuthenticationController {
    private final SystemUserService userService;
   private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;


    @PostMapping("/signup")
    public ResponseEntity<SystemUser> create( @Valid @RequestBody SystemUser user) {
        return ResponseEntity.ok().body(userService.createUser(user));
    }

    @PutMapping("/{id}")
    public ResponseEntity<SystemUser> update(@PathVariable final Long id, @Valid @RequestBody SystemUser user) {
        return ResponseEntity.ok().body(userService.update(id, user));
    }

    @PutMapping("/role/{id}/{roleType}")
    public ResponseEntity<SystemUser> updateRole(@PathVariable final Long id, @Valid @PathVariable UserRoleType roleType) {
        return ResponseEntity.ok().body(userService.updateRole(id, roleType));
    }

    @PutMapping("/password/{id}/{password}")
    public ResponseEntity<SystemUser> updatePassword(@PathVariable final Long id, @Valid @PathVariable String password) {
        return ResponseEntity.ok().body(userService.updatePassword(id, password));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable final Long id) {
        return ResponseEntity.ok().body(userService.delete(id));
    }

    @GetMapping()
    @PreAuthorize("hasRole('" + UserRoleType.Code.ROLE_SUPER_ADMIN + "')")
    public ResponseEntity<List<SystemUser>> getAllUsers() {
        return ResponseEntity.ok().body(userService.getAllUsers());
    }

    @PostMapping("/login")
    public ResponseEntity<UserDetails> login(HttpServletRequest request, @Valid @RequestBody LoginRequest login) {

        String username = login.getUserName();
        String password = login.getPassword();

        log.info("Trying to login " + username + " from " + request.getRemoteAddr());

        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);

        // if request is needed during authentication
        token.setDetails(new WebAuthenticationDetails(request));
        try {

            Authentication auth = authenticationManager.authenticate(token);

            log.info("Authorities for " + username + ": " + auth.getAuthorities().stream().map(Object::toString).collect(Collectors.joining(",")));

            SecurityContext securityContext = SecurityContextHolder.getContext();
            securityContext.setAuthentication(auth);


            // if user has a http session we need to save the context in the session for next requests
            HttpSession session = request.getSession(true);
            session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);

            var user = userService.loadUserByUsername(username);


            log.info("User " + user.getUsername() + ": login successful");
            log.info( "Bearer " + jwtTokenUtil.generateToken(user.getUsername()));
            return ResponseEntity.ok((UserDetails) auth.getPrincipal());
        } catch (AuthenticationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage(), e);
        }
    }


    @PostMapping("/logout")
    public ResponseEntity logout(HttpServletRequest request, HttpServletResponse response) {
        userService.logout(request, response);
        return  ResponseEntity.ok().build();

    }

}

