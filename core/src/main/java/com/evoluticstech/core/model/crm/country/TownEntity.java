package com.evoluticstech.core.model.crm.country;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;

/**
 * @author Brume
 **/

@Data
@NoArgsConstructor
@Entity
@Table(name = "TOWN", schema = "CRM", catalog = "EVOLUTICS")
public class TownEntity {
    @Column(name = "CODE", nullable = true, length = 255)
    private String code;

    @Column(name = "DESCRIPTION", nullable = true, length = 255)
    private String description;

    @Column(name = "CITY", nullable = true, length = 255)
    private String city;

    @Column(name = "REGION", nullable = true, length = 255)
    private String region;

    @Column(name = "COUNTRY", nullable = true, length = 255)
    private String country;

    @Column(name = "ID", nullable = false)
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
}
