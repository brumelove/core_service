package com.evoluticstech.core.model.crm.country;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;

/**
 * @author Brume
 **/

@Data
@NoArgsConstructor
@Entity
@Table(name = "COUNTRY", schema = "CRM", catalog = "EVOLUTICS")
public class CountryEntity {
    @Column(name = "ADDRESS_FORMAT", nullable = true, length = 255)
    private String addressFormat;

    @Column(name = "ALPHA3_CODE", nullable = true, length = 255)
    private String alpha3Code;

    @Column(name = "BANKING_METHOD", nullable = true, length = 255)
    private String bankingMethod;

    @Column(name = "BANK_ACCOUNT_MAX_LENGTH", nullable = true, length = 255)
    private String bankAccountMaxLength;

    @Column(name = "BANK_ACCOUNT_MIN_LENGTH", nullable = true, length = 255)
    private String bankAccountMinLength;

    @Column(name = "CODE", nullable = false, length = 255)
    private String code;

    @Column(name = "ISO_NUMBER", nullable = true, precision = 0)
    private Double isoNumber;

    @Column(name = "DESCRIPTION", nullable = true, length = 255)
    private String description;

    @Column(name = "PREMIUM_TAX_TABLE", nullable = true, length = 255)
    private String premiumTaxTable;

    @Column(name = "POSTCODE_PREFIX", nullable = true, length = 255)
    private String postcodePrefix;

    @Column(name = "POST_CODE_LOOKUP_ALLOWED", nullable = true, length = 255)
    private String postCodeLookupAllowed;

    @Column(name = "PREMIUM_TAX_RATE_TABLE", nullable = true, length = 255)
    private String premiumTaxRateTable;

    @Column(name = "SORT_CODE_OPTIONAL", nullable = true, length = 255)
    private String sortCodeOptional;

    @Column(name = "VALIDATE_SORT_CODE", nullable = true)
    private Boolean validateSortCode;

    @Column(name = "TT_COUNTRY_NAME", nullable = true, length = 255)
    private String ttCountryName;

    @Column(name = "DIRECT_DEBIT_METHOD", nullable = true, length = 255)
    private String directDebitMethod;

    @Column(name = "ACC_VALIDATION", nullable = true, length = 255)
    private String accValidation;

    @Column(name = "COUNTRY_GROUP", nullable = true, length = 255)
    private String group;

    @Column(name = "PHONE_NO_BASIS", nullable = true, length = 255)
    private String phoneNoBasis;

    @Column(name = "PHONE_NO_LENGTH", nullable = true)
    private Integer phoneNoLength;

    @Column(name = "SORT_CODE_BASIS", nullable = true, length = 255)
    private String sortCodeBasis;

    @Column(name = "DEFAULT_LANGUAGE", nullable = true, length = 255)
    private String defaultLanguage;

    @Column(name = "PHONE_NO_PREFIX", nullable = true)
    private Integer phoneNoPrefix;

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;
}
