package com.evoluticstech.core.model.crm.user;

import com.evoluticstech.core.config.AuditModel;
import com.evoluticstech.core.enumerations.UserRoleType;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Brume
 */
@Entity
@Data
@NoArgsConstructor
@Table(name = "SYS_USERS", schema = "CRM")
public class SystemUserEntity extends AuditModel {
    @Column(name = "EMAIL")
    private String email;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "SURNAME")
    private String surname;

    @Column(name = "FIRSTNAME")
    private String firstname;

    @Column(name = "USERNAME")
    private String userName;

    @Column(name = "ROLE")
    @Enumerated(EnumType.STRING)
    private UserRoleType userRoleType;

}
