package com.evoluticstech.core.model.crm.country;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;

/**
 * @author Brume
 **/

@Data
@NoArgsConstructor
@Entity
@Table(name = "CITY", schema = "CRM", catalog = "EVOLUTICS")
public class CityEntity {
    @Column(name = "CODE", nullable = true, length = 255)
    private String code;

    @Column(name = "DESCRIPTION", nullable = true, length = 255)
    private String description;

    @Column(name = "COUNTRY", nullable = true, length = 255)
    private String country;

    @Column(name = "REGION", nullable = true, length = 255)
    private String region;

    @Column(name = "ID", nullable = false)
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
}
