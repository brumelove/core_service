package com.evoluticstech.core.model.crm.country;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name= "COUNTRY_LANG", schema = "CRM")
public class CountryLangEntity
{
	@Column(name = "ID")
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "COUNTRY_CODE")
	private String code;
	
	@Column(name = "LANGUAGE")
	private String language;
}