package com.evoluticstech.core.interfaces;

/**
 * @author Brume
 **/
public interface Group {
    String getGroup();
}
