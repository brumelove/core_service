package com.evoluticstech.core.interfaces;

/**
 * @author Brume
 **/
public interface CodeAndDescription {
    String getCode();
    String getDescription();
}
