package com.evoluticstech.core.interfaces;

/**
 * @author Brume
 **/
public interface CodeAndTitle {
    String getCode();
    String getTitle();
}
