package com.evoluticstech.core.interfaces;

/**
 * @author Brume
 **/
public interface CodeAndDescriptionAndIsoNumberAndId {
    String getCode();
    String getDescription();
    Double getIsoNumber();
    Long getId();
}
