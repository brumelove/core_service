package com.evoluticstech.core.domain.users;

import lombok.Data;

/**
 * Brume
 */
@Data
public class LoginRequest {
    private String password;
    private String userName;
}
