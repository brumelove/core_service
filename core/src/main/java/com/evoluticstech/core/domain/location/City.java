package com.evoluticstech.core.domain.location;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Brume
 **/
@Data
@NoArgsConstructor
public class City {
    private String code;
    private String description;
    private String country;
    private String region;
    @ApiModelProperty(hidden = true)
    private Long id;
}
