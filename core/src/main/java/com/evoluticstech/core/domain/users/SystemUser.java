package com.evoluticstech.core.domain.users;

import com.evoluticstech.core.enumerations.UserRoleType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Brume
 */
@Data
public class SystemUser {
    @ApiModelProperty(hidden = true)
    private Long id;
    private String email;
    private String password;
    private String surname;
    private String firstname;
    private String userName;
    @ApiModelProperty(hidden = true)
    private UserRoleType userRoleType = UserRoleType.USER;
}
