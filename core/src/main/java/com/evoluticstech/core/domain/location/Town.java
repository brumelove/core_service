package com.evoluticstech.core.domain.location;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Brume
 **/
@Data
@NoArgsConstructor
public class Town {
    private String code;
    private String description;
    private String city;
    private String region;
    private String country;
    @ApiModelProperty(hidden = true)
    private Long id;
}