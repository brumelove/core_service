package com.evoluticstech.core.domain.location;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Brume
 **/
@Data
@NoArgsConstructor
public class Country {
    private String addressFormat;
    private String alpha3Code;
    private String bankingMethod;
    private String bankAccountMaxLength;
    private String bankAccountMinLength;
    private String code;
    private Double isoNumber;
    private String description;
    private String premiumTaxTable;
    private String postcodePrefix;
    private String postCodeLookupAllowed;
    private String premiumTaxRateTable;
    private String sortCodeOptional;
    private Boolean validateSortCode;
    private String ttCountryName;
    private String directDebitMethod;
    private String accValidation;
    private String group;
    private String phoneNoBasis;
    private Integer phoneNoLength;
    private String sortCodeBasis;
    private Integer phoneNoPrefix;
    @ApiModelProperty(hidden = true)
    private Long id;
}
