package com.evoluticstech.core.domain.location;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CountryLang {
    @ApiModelProperty(hidden = true)
    public Long id;
    private String code;
    private String language;
}
