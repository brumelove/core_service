package com.evoluticstech.core.enumerations;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Brume
 */

@AllArgsConstructor
public enum UserRoleType {
    USER(Code.ROLE_USER),
    SUPER_USER(Code.ROLE_SUPER_USER),
    ADMIN(Code.ROLE_ADMIN),
    SUPER_ADMIN(Code.ROLE_SUPER_ADMIN);

    @Getter
    private final String userType;
    
    public static class Code {
        public static final String ROLE_STAFF = "bSTAFF";
        public static final String ROLE_SUPER_ADMIN = "ROLE_SUPER_ADMIN";
        public static final String ROLE_ADMIN = "ADMIN";
        public static final String ROLE_CEO_OFFICE = "ROLE_CEO-OFFICE";
        public static final String ROLE_USER = "USER";
        public static final String ROLE_SUPER_USER = "SUPER_USER";
        public static final String ROLE_CB_TRAINERS = "ROLE_CB_TRAINERS";
        public static final String ROLE_SYSTEM_ADMINS = "ROLE_SYSTEM-ADMINS";
        public static final String ROLE_IT_ENGINEERING = "ROLE_IT-ENGINEERING";
        public static final String ROLE_MEMBER_SERVICES = "ROLE_MEMBER-SERVICES";
    }
}
