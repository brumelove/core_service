Core project for EVOLUTICS written in JAVA 11.

SOLID PRINCIPLES GUIDING THIS PROJECT

A package should never contain more than one prefix,
For example, package crm.model.covers must only contain table names with the prefix : COVERS,
package crm.model.users must only contain table names with the prefix : USERS

NEVER EVER NAME with CAPITAL LETTERS, Your Naming convention should be camelCase.
If you are making use of any plugin please demonstrate due diligence by
removing unnecessary getters and setters method if and only if an annotation is used.
@Getter and @Setter == @Data, they can be used interchangeably but never together. 

Your Model should follow the Naming conversion of TableName + Entity;
Your Repository interface should follow the Naming conversion of TableName + Repository;
Your Service class layer should follow the Naming conversion of TableName + Service;
Your Controller class layer should follow the Naming conversion of TableName + Controller;

Every Model MUST have a Domain Class.
Every Model MUST have its repository.
Every Repository MUST have its Service Class.

Do not inject more than one repository interface for a service class,
You should inject service classes into whatever layer you want, not repository classes.
Do not expose the Entity classes to the controller class.
Your Entity or Model classes should be used only in the repository interface.
Please ensure that the Domain class is what is exposed by the service class to the controller.
